# Import required Packages
import pandas as pd
import pickle
import numpy as np

class DET_Model():
    def __init__(self):
        self.sites = {}

    def load_data(self, file):
        try:
            self.sites = pickle.load(open(f'./Data/sites.p', 'rb'))

        except:
            # Generate Site objects and their details
            for i in pd.read_csv(file, dtype=str).iterrows():
                current_site = Site(i[1]['UseID'], int(i[1]['Started']), i[1]['Site_Name'])
                pop_list = i[1][3:]
                for e in pop_list.keys():
                    year, pop_type = e.split('-')
                    pop = float(pop_list[e])
                    if pop_type == 'Students':
                        current_site.add_student_population(int(year), pop)
                    if pop_type == 'Administrators':
                        current_site.add_administrator_population(int(year), pop)
                    if pop_type == 'Teachers':
                        current_site.add_teacher_population(int(year), pop)

                self.sites[current_site.useid] = current_site
    
    def save_data(self):
        pickle.dump( self.sites, open(f'./Data/sites.p', 'wb'))

class Site():
    ''' Class for a site object. All population needs to be added consecutively'''
    
    def __init__(self, useid, year_started, site_name="unknown"):
        self.useid = useid
        self.site_name = site_name
        self.devices = []
        self.student_non_support_population = {}
        self.student_support_population = {}
        self.student_distance_education_non_support_population = {}
        self.student_distance_education_support_population = {}
        self.administrator_population = {}
        self.teacher_population = {}
        self.year_started = year_started
        self.max_student_pop = 0
        self.max_teacher_pop = 0
        self.max_administrator_pop = 0
        self.cer_students_per_device_options = {2019: 6, 2020: 6, 2021: 6, 2022: 6}
        self.establishment_students_per_device_options = {2019: 1, 2020: 1, 2021: 1, 2022: 1}
        self.establishment_students_per_library_device_options = {2019: 100, 2020: 100, 2021: 100, 2022: 100}
        self.establishment_teachers_per_device_options = {2019: 1, 2020: 1, 2021: 1, 2022: 1}
        self.establishment_existing_devices_per_swapout_device_options = {2019: 250, 2020: 250, 2021: 250, 2022: 250}
        self.establishment_administrators_per_device_options = {2019: 1, 2020: 1, 2021: 1, 2022: 1}

    def active_device_count(self, obj, year):
        '''
        Counts the number of devices

        param
        obj is a list of the name of the device type desired to count
        year is the year the count should not exceed
        '''

        count = 0
        for dev in self.devices:
            if type(dev) in obj and dev.year_added <= year and dev.life_expiry_year >= year:
                count += 1

        return count

    def add_student_population(self, year, pop):
        self.student_non_support_population[year] = pop
        self.add_establishment_student_devices(year, pop)
        self.add_established_library_devices(year, pop)
        self.add_established_swapout_devices(year, pop)
        self.add_cer_student_devices(year, pop)
        if self.max_student_pop < pop:
            self.max_student_pop = pop

    def add_teacher_population(self, year, pop):
        self.teacher_population[year] = pop
        self.add_established_teacher_devices(year, pop)
        if self.max_teacher_pop < pop:
            self.max_teacher_pop = pop

    def add_administrator_population(self, year, pop):
        self.administrator_population[year] = pop
        self.add_established_administrator_devices(year, pop)
        if self.max_administrator_pop < pop:
            self.max_administrator_pop = pop

    def add_devices(self, num, year, dev_type):
        """
        Can be used to add devices manually if needed
        """

        for _ in range(num):
            self.devices.append(dev_type(year))

    def add_cer_student_devices(self, year, pop):
        """ 
        This add devices applied using a CER algorithm. This algorithm is currently incomplete as it doesn't factor in things like support students
        """
        
        # Students per Device Ratio
        if year in self.cer_students_per_device_options.keys():
            students_per_dev = self.cer_students_per_device_options[year]
        else:
            students_per_dev = self.cer_students_per_device_options[list(self.cer_students_per_device_options.keys())[-1]]

        # CER distribution Rules
        cer_odd_year_distribution = 0.22 # applied in odd years
        cer_odd_year_distribution_small = 0.5 # applied in odd years for sites <160 enrolments
        cer_even_year_distribution = 0.28 # applied in even years

        dist = (cer_odd_year_distribution_small if (int(year) % 2 == 1 and pop < 160) else cer_odd_year_distribution if int(year) % 2 == 1 
        else cer_even_year_distribution if (int(year) % 2 == 0 and pop > 159) else 0)

        # Create number of devices if appropriate time
        if abs(self.year_started - year) >= 0:
            dev_num = int(np.ceil(pop / students_per_dev * dist))
            for _ in range(dev_num):
                self.devices.append(CER_Student_Virtual_Device(year))

    def add_establishment_student_devices(self, year, pop):
        
        # Students per Device Ratio
        if year in self.establishment_students_per_device_options.keys():
            students_per_dev = self.establishment_students_per_device_options[year]
        else:
            students_per_dev = self.establishment_students_per_device_options[list(self.establishment_students_per_device_options.keys())[-1]]

        # Create number of devices if appropriate time
        if abs(int(self.year_started) - int(year)) < 5:
            dev_num =  int(np.ceil(pop / students_per_dev)) - self.active_device_count([Establishment_Student_Virtual_Device], year)
            if dev_num <= 0:
                pass
            else:
                for _ in range(dev_num):
                    self.devices.append(Establishment_Student_Virtual_Device(year))

    def add_established_library_devices(self, year, pop):
        
        # Students per Device Ratio
        if year in self.establishment_students_per_library_device_options.keys():
            students_per_dev = self.establishment_students_per_library_device_options[year]
        else:
            students_per_dev = self.establishment_students_per_library_device_options[list(self.establishment_students_per_library_device_options.keys())[-1]]

        # Create number of devices if appropriate time
        if abs(int(self.year_started) - int(year)) < 1:
            dev_num = int(np.ceil(pop / students_per_dev) + 2) - self.active_device_count([Establishment_Library_Virtual_Device], year)
            if dev_num <= 0:
                pass
            else:
                for _ in range(dev_num):
                    self.devices.append(Establishment_Library_Virtual_Device(year))

    def add_established_swapout_devices(self, year, pop):
        
        # dev_count_total = 0
        # dev_count_previous = 0


        # for dev in self.devices:
        #     if dev.year_added < year and dev.supplied_by == 'Establishment' and dev.assigned_to == 'Swapout':
        #         dev_count_previous += 1
        #     if dev.year_added <= year and dev.supplied_by == 'Establishment' and (dev.assigned_to == 'Student' or dev.assigned_to == 'Library'):
        #         dev_count_total += 1

        # Existing Devices per Swapout Device
        if year in self.establishment_existing_devices_per_swapout_device_options.keys():
            existing_devices_per_swapout_device = self.establishment_existing_devices_per_swapout_device_options[year]
        else:
            existing_devices_per_swapout_device = self.establishment_existing_devices_per_swapout_device_options[list(self.establishment_existing_devices_per_swapout_device_options.keys())[-1]]

        # replace_for_every = 250

        # Create number of devices if appropriate time
        if abs(int(self.year_started) - int(year)) < 5:
            dev_num =  int(np.ceil(self.active_device_count([Establishment_Student_Virtual_Device, Establishment_Library_Virtual_Device], year) / existing_devices_per_swapout_device) * 2) - self.active_device_count([Establishment_Swapout_Virtual_Device], year)
            if dev_num <= 0:
                pass
            else:
                for _ in range(dev_num):
                    self.devices.append(Establishment_Swapout_Virtual_Device(year))

    def add_established_administrator_devices(self, year, pop):

        # Administrators per Device Ratio
        if year in self.establishment_administrators_per_device_options.keys():
            administrators_per_dev = self.establishment_administrators_per_device_options[year]
        else:
            administrators_per_dev = self.establishment_administrators_per_device_options[list(self.establishment_administrators_per_device_options.keys())[-1]]

        # Create number of devices if appropriate time
        if abs(int(self.year_started) - int(year)) < 5:
            dev_num =  int(np.ceil(pop / administrators_per_dev)) - self.active_device_count([Establishment_Administrator_Virtual_Device], year)
            if dev_num <= 0:
                pass
            else:
                for _ in range(dev_num):
                    self.devices.append(Establishment_Administrator_Virtual_Device(year))

    def add_established_teacher_devices(self, year, pop):
        
        # Teachers per Device Ratio
        if year in self.establishment_teachers_per_device_options.keys():
            teachers_per_dev = self.establishment_teachers_per_device_options[year]
        else:
            teachers_per_dev = self.establishment_teachers_per_device_options[list(self.establishment_teachers_per_device_options.keys())[-1]]

        # Create number of devices if appropriate time
        if abs(int(self.year_started) - int(year)) < 5:
            dev_num =  int(np.ceil(pop / teachers_per_dev)) - self.active_device_count([Establishment_Teacher_Virtual_Device], year)
            if dev_num <= 0:
                pass
            else:
                for _ in range(dev_num):
                    self.devices.append(Establishment_Teacher_Virtual_Device(year))

    def display_counts(self):
        print("----------------------")
        print(f'Site Name: {self.site_name}')
        print('Populations and Device totals were:')
        for year in self.student_non_support_population.keys():
            total_count_dev = 0
            count_dev_student = 0
            count_dev = 0
            removed_dev = 0
            in_warranty = 0
            out_warranty = 0
            current_cost = 0
            for dev in self.devices:
                if dev.year_added <= year and dev.life_expiry_year >= year:
                    total_count_dev += 1
                    current_cost += dev.cost
                if dev.year_added <= year and dev.life_expiry_year >= year and dev.assigned_to == "Student":
                    count_dev_student += 1
                if dev.year_added == year:
                    count_dev += 1
                if dev.life_expiry_year == year - 1:
                    removed_dev += 1
                if dev.year_added <= year and dev.life_expiry_year >= year and dev.warranty_expiry_year < year:
                    out_warranty += 1
                if dev.year_added <= year and dev.life_expiry_year >= year and dev.warranty_expiry_year >= year:
                    in_warranty += 1
            ratio = (self.student_non_support_population[year]/count_dev_student) if count_dev_student != 0 else 0
            print(f'{year}: {self.student_non_support_population[year]} students, {self.administrator_population[year]} administrators, {self.teacher_population[year]} teachers \n{total_count_dev} total devices of value ${current_cost} of which {count_dev_student} devices are assigned to students, {count_dev} new devices, {in_warranty} devices in warranty (including new), {out_warranty} devices out of warranty, {removed_dev} were decommissioned (not in current total), {ratio} students to devices')
    
    def display_current(self):
        year = max(self.student_non_support_population.keys())
        print("----------------------")
        print(f'Current Year: {year}')
        print(f'Site Name: {self.site_name}')
        print(f'Year Started: {self.year_started}')
        print(f'Student Count: {self.student_non_support_population[year]}')
        
        in_warranty = 0
        out_warranty = 0
        total_devices = 0
        for dev in self.devices:
            if dev.year_added <= year and dev.life_expiry_year >= year and dev.warranty_expiry_year < year:
                out_warranty += 1
            if dev.year_added <= year and dev.life_expiry_year >= year and dev.warranty_expiry_year >= year:
                in_warranty += 1
            if dev.year_added <= year and dev.life_expiry_year >= year:
                total_devices += 1
        ratio = self.student_non_support_population[year]/total_devices

        print(f"Current Students per Device: {ratio}")
        print("Device Breakdown:")
        print(f"{total_devices} Devices")
        print(f"- {in_warranty} In Warranty")
        print(f"- {out_warranty} Out of Warranty")

# Super and Sub Objects for Devices

class Device():
    ''' Generic Device Class used as base class for both real and virtual devices '''

    def __init__(self, year_added, warranty=3, life=5):
        self.supplied_by = 'Unknown'
        self.assigned_to = 'Unknown'
        self.Vendor = 'Unknown'
        self.Make = 'Unknown'
        self.cost = float(0.00)
        self.year_added = year_added
        self.life = life
        self.warranty = warranty
        self.warranty_expiry_year = self.year_added + self.warranty - 1
        self.life_expiry_year = self.year_added + self.life - 1     

class CER_Student_Virtual_Device(Device):
    def __init__(self, year, warranty=2, life=3):
        super().__init__(year, warranty, life)
        
        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(600.00), 2020: float(600.00), 2021: float(600.00), 2022: float(600.00), 2023: float(600.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'CER'
        self.assigned_to = 'Student'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

class Establishment_Student_Virtual_Device(Device):
    def __init__(self, year, warranty=2, life=3):
        super().__init__(year, warranty, life)

        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(800.00), 2020: float(800.00), 2021: float(800.00), 2022: float(800.00), 2023: float(800.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'Establishment'
        self.assigned_to = 'Student'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

class Establishment_Administrator_Virtual_Device(Device):
    def __init__(self, year, warranty=4, life=6):
        super().__init__(year, warranty, life)

        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(600.00), 2020: float(600.00), 2021: float(600.00), 2022: float(600.00), 2023: float(600.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'Establishment'
        self.assigned_to = 'Administrator'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

class Establishment_Library_Virtual_Device(Device):
    def __init__(self, year, warranty=2, life=3):
        super().__init__(year, warranty, life)

        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(1200.00), 2020: float(1200.00), 2021: float(1200.00), 2022: float(1200.00), 2023: float(1200.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'Establishment'
        self.assigned_to = 'Library'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

class Establishment_Swapout_Virtual_Device(Device):
    def __init__(self, year, warranty=2, life=3):
        super().__init__(year, warranty, life)

        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(800.00), 2020: float(800.00), 2021: float(800.00), 2022: float(800.00), 2023: float(800.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'Establishment'
        self.assigned_to = 'Swapout'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

class Establishment_Teacher_Virtual_Device(Device):
    def __init__(self, year, warranty=4, life=6):
        super().__init__(year, warranty, life)

        # Device Cost needs to be added to dictionary for particular years
        device_cost = {2019: float(1300.00), 2020: float(1300.00), 2021: float(1300.00), 2022: float(1300.00), 2023: float(1300.00)}

        if year in device_cost.keys():
            cost = device_cost[year]
        else:
            cost = float(0.00)

        self.supplied_by = 'Establishment'
        self.assigned_to = 'Teacher'
        self.Vendor = 'Virtual'
        self.cost = float(cost)

# Reports for extracting data for external use in spreadsheets or display

class Report():
    def __init__(self, obj_list, start_year=None, end_year=None, site_list=[]):
        self.obj_list = obj_list
        self.start_year = start_year
        self.end_year = end_year
        self.site_list = site_list

    def devices(self, Vendor=None):
        report = {"UseID": [], "Supplied_by": [], "Assigned_to": [], "Vendor": [], "Year_added": [], "Warranty_Expiry_Year": [], "Life_expiry_year": [], "Cost": []}

        def extract():
            report["UseID"].append(obj)
            report["Supplied_by"].append(e.supplied_by)
            report["Assigned_to"].append(e.assigned_to)
            report["Vendor"].append(e.Vendor)
            report["Year_added"].append(e.year_added)
            report["Warranty_Expiry_Year"].append(e.warranty_expiry_year)
            report["Life_expiry_year"].append(e.life_expiry_year)
            report["Cost"].append(e.cost)


        if self.site_list == []:
            for obj in self.obj_list.keys():
                for e in self.obj_list[obj].devices:
                    if Vendor == None:
                        extract()
                    elif e.Vendor in Vendor:
                        extract()
        else:
            for obj in self.site_list:
                for e in self.obj_list[obj].devices:
                    if Vendor == None:
                        extract()
                    elif e.Vendor in Vendor:
                        extract()
                    
                        
        temp_df = pd.DataFrame(report)
        temp_df.to_csv('./Data/devices.csv')

    def site_selection(self):
        while True:
            import os
            os.system('cls')
            print(self.obj_list.keys())
            response = input("What site code would you like to look at: ")
            if response in self.obj_list.keys():
                os.system('cls')
                site = self.obj_list[response]
                site.display_counts()
                site.display_current()
                print("--------------------------")
                input("Press Enter to continue")
                
            elif response.lower() == 'x':
                break
            else:
                print('I don\'t have that code. Please try again')