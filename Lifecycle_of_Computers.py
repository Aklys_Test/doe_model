# Import required Packages

import pandas as pd
import pickle
import DET_Model

def main():
    obj = DET_Model.DET_Model()
    obj.load_data('./Data/Schools.csv')

    # school = obj.sites['4663']

    # print(school.active_device_count(DET_Model.Establishment_Swapout_Virtual_Device, 2019))

    report = DET_Model.Report(obj.sites)
    report.site_selection()
    report.devices()

if __name__ == "__main__":
    main()

